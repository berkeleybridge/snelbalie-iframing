# snelbalie-iframing

Scripts en handleiding (deze README) om de Snelbalie te i-framen in eigen gemeentelijke site.

Vervang hieronder telkens 'snelbalie.gemeente.nl' met de plek waar de snelbalie daadwerkelijk draait.

# Sitebeheerders en web developer Berkeley Bridge stemmen de te gebruiken URLs af

1. child: URL waar Snelbalie zelf draait, bv. `https://gemeente.bridge-to-knowledge.nl` of `https://snelbalie.gemeente.nl`
2. parent: URL waar Snelbalie ge-iframed wordt, bv. `https://www.gemeente.nl/balie`

Hieronder staan onder Parent de acties die de sitebeheerder moet ondernemen, en onder child de acties die de web developers van Berkeley Bridge moeten ondernemen.

# Child

## Pas configuratie van de webomgeving (`config.json`) aan:

    {
      ....
      "arbitrary": {
	    "parentDomain" : 'https://parent.gemeente.nl'
       }
	 }

Bak de omgeving en zet hem neer (als gehost bij Berkeley Bridge)/lever hem aan applicatiebeheerder.

# Parent

## Zorg ervoor dat de CSP header goed staat

Bijvoorbeeld:

`<meta http-equiv="Content-Security-Policy" content="frame-src 'self' https://snelbalie.gemeente.nl">`

## Neem de volgende iframe-code op op de site

    <iframe id="snelbalie" src="https://snelbalie.gemeente.nl" style="height: 3700px;">
      <a href="https://snelbalie.gemeente.nl">open de Snelbalie</a>
    </iframe>`

Het id (`snelbalie`) zorgt ervoor dat het resize script het `iframe` vinden kan.

Zorg ervoor de Snelbalie ook te ontsluiten als `iframes` geblokkeerd of niet ondersteund worden door een link (`<a>`)
op te nemen naar dezelfde content ([Voldoen aan succescriterium U.7.1 middels afdoende techniek](http://versie2.webrichtlijnen.nl/techniek/Hu4/)).
Dit kan zowel binnen als buiten het iframe.

**NB** Pas `src` en `href` aan naar het afgestemde child url.

## Neem een resize script op op de pagina waar de Snelbalie ge-iframed wordt

Het `iframe` zal elke keer dat het een andere hoogte nodig heeft een `message` sturen naar de pagina met het `iframe`.

In `resize-scripts` zijn scripts te vinden die reageren op het `message` ervoor zorgen dat deze ruimte aan het `iframe` gegeven wordt.

### resize-scripts/resize-same-domain.js

Gebruik deze als de pagina met het `iframe` en de Snelbalie allebei op een subdomein draaien van hetzelfde domein en allebei onder hetzelfde protocol draaien (https).

### resize-scripts/resize-one-domain.js

Gebruik dit script als de pagina met het `iframe` en de Snelbalie *niet* op een subdomein draaien van hetzelfde domein of als je de communicatie verder wilt beperken.

**NB** Dit script moet wel eerst aangepast worden (de waarde van `origin` op regel 3) om hardcoded de origin te checken.

### resize-scripts/resize-all-domains.js

Dit script voert *geen check* uit op de origin van het message.

Gebruik dit script om te testen.
