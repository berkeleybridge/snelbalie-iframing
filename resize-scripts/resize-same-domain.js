(function(){
  var id = 'snelbalie';

  function setHeightOnIframe(height){
    var iframe = document.getElementById(id);
    iframe.style.height = height + 'px';
  }

  function protocol(origin){
    return origin.split('//')[0];
  }

  function domain(origin){
    return origin.split(/\/\./).slice(1).join('.');
  }

  function areInSameDomainAndProtocol(a, b){
    return ( (domain(a) === domain(b) ) && ( protocol(a) === protocol(b) ) );
  }

  function receiveMessage(event)
  {
    // Do we trust the sender of this message?  (might be
    // different from what we originally opened, for example).
    var jsondata;
    if (areInSameDomainAndProtocol(event.origin, window.location.origin))
      return;
    if (event.data) {
      try {
        jsondata = JSON.parse(event.data);
      } catch(e) {
      } finally {
        if (jsondata) {
          setHeightOnIframe(jsondata.height);
        }
      }
    }
  }


  if (window.addEventListener) {
    window.addEventListener('message', receiveMessage, false);
  } else if (window.attachEvent) {
    window.attachEvent('onmessage', receiveMessage);
  }

})();
