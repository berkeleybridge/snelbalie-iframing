(function(){
  var id = 'snelbalie',
      origin = 'https://gemeente.bridge-to-knowledge.nl';

  function setHeightOnIframe(height){
    var iframe = document.getElementById(id);
    iframe.style.height = height + 'px';
  }

  function receiveMessage(event)
  {
    // Do we trust the sender of this message?  (might be
    // different from what we originally opened, for example).
    var jsondata;
    if (event.origin !== origin)
      return;
    if (event.data) {
      try {
        jsondata = JSON.parse(event.data);
      } catch(e) {
      } finally {
        if (jsondata) {
          setHeightOnIframe(jsondata.height);
        }
      }
    }
  }


  if (window.addEventListener) {
    window.addEventListener('message', receiveMessage, false);
  } else if (window.attachEvent) {
    window.attachEvent('onmessage', receiveMessage);
  }

})();
